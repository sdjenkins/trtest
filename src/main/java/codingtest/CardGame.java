package codingtest;

import java.util.Arrays;

import codingtest.controller.CardGameController;
import codingtest.service.BlackjackService;

/**
 * Class that handles the playing of a card game from a simple command line interface,
 * and echoes back a step-by-step description of the game to the console.
 */
public class CardGame {
	//TODO - comments in all classes
    /**
     * Main. Plays a card game from a command line interface.
     * @param args the arguments to the game
     */
    public static void main(String[] args) {
    	
    	//TODO - parameters from command line
    	CardGameController controller = new CardGameController();
    	controller.setService(new BlackjackService());
    	controller.setPlayers(3);
    	controller.play();	
    }
}
