package codingtest.controller;

import codingtest.domain.Deck;
import codingtest.domain.Player;
import codingtest.service.CardGameService;
import codingtest.service.GameOverException;

public class CardGameController {

	private int players;
	private CardGameService service;
	private Object shuffleMethod; //TODO create interface for this
	
	public void setShuffleMethod(Object shuffleMethod) {
		this.shuffleMethod = shuffleMethod;
	}

	public void setPlayers(int players) {
		this.players = players;
	}

	public void setService(CardGameService service) {
		this.service = service;
	}

	public void play() {

		Player[] playerGroup = new Player[players];
		Deck deck = Deck.getStandard52CardDeck();
		deck.shuffle(); // Always do initial shuffle??

		//TODO - Apply shuffle method
		
		for (int i = 0; i < players; i++) {
			playerGroup[i] = new Player("Player: " + (i + 1));
		}

		service.dealHands(playerGroup, deck);

		//TODO - re-factor for readability
		int player = 0;
		boolean isWinner;
		try {
			isWinner = service.takeTurn(playerGroup[player], deck);
			while (isWinner == false) {
				player = ++player % players;
				isWinner = service.takeTurn(playerGroup[player], deck);
			}
		} catch (GameOverException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		service.declareWinner(playerGroup);
	}
}
