package codingtest.domain;

/**
 * This is the domain class that represents a card in a card game.
 */
public class Card {
	
	private Suit suit;
	private Rank rank;
	private String symbol;
	
	public Card(Rank rank, Suit suit, String symbol) {
		super();
		this.suit = suit;
		this.rank = rank;
		this.symbol = symbol;
	}
	
	public Card(Rank rank, Suit suit) {
		
		this(rank, suit, " " + rank + suit);
	}
	
	public Suit getSuit() {
		return suit;
	}
	
	public Rank getRank() {
		return rank;
	}
	
	@Override
	public String toString() {
		return symbol;
	}
}
