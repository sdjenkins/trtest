package codingtest.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * This is the class that represents a deck of cards in a card game.
 */

//TODO - Add shuffle method that takes Interface argument.
public class Deck {
	
	private List<Card> cards = new ArrayList<>();
	
	public static Deck getStandard52CardDeck() {
		
		Deck deck = new Deck();
		for (Suit suit : Suit.values()) {
			for (Rank rank : Rank.values()) {
				deck.addCard(new Card(rank, suit));
			}
		}
		return deck;
	}
	
	@Override
	public String toString() {
		return cards.toString();
	}

	private void addCard(Card card) {
		
		cards.add(card);
	}
	
	public Deck(Card[] contents) {
		
		cards.addAll(Arrays.asList(contents));
	}
	
	private Deck() {
	}
	
	public Card dealNextCard() {
		
		return cards.remove(0);
	}
	
	public boolean isEmpty() {
		
		return cards.size() == 0;
	}
	
	public void shuffle() {
		
		Collections.shuffle(cards);
	}
	
}
