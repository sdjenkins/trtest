package codingtest.domain;

import java.util.ArrayList;
import java.util.List;

public class Player {
	
	private String name;
	private List<Card> hand = new ArrayList<Card>();
	private int value;
	
	public Player(String name) {
		this.name = name;
	}
	
	public List<Card> getHand() {
		return hand;
	}
	
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return name + ":  " + hand;
	}
	
	
}
