package codingtest.domain;

public enum Suit {
	CLUBS("♣"), HEARTS("♥"), DIAMONDS("♦"), SPADES("♠");
	
	private String symbol;
	private Suit(String symbol) {
		this.symbol = symbol;
	}
	
	@Override
	public String toString() {
		return symbol;
	}
}
