package codingtest.service;

import java.util.List;

import codingtest.domain.Card;
import codingtest.domain.Deck;
import codingtest.domain.Player;
import codingtest.domain.Rank;

public class BlackjackService implements CardGameService {

	@Override
	public void dealHands(Player[] players, Deck deck) {
		
		System.out.println("\nDealing...\n");
		for (int i = 0; i < 2; i++) {
			for (Player player : players) {
				player.getHand().add(deck.dealNextCard());
				System.out.println(player);
			}
		}
	}

	@Override
	public boolean takeTurn(Player player, Deck deck) throws GameOverException {
		
		if (player.getValue() > 0) {
			throw new GameOverException();
		}
		
		System.out.println("\nPlaying turn...");
		System.out.print(player);
		List<Card> hand = player.getHand();
		int value = computeValue(hand);
		
		while (value < 17 && value <= 21) {
			System.out.println(" - Hit...");
			player.getHand().add(deck.dealNextCard());
			System.out.print(player);
			value = computeValue(hand);
		}
		
		if (value > 21) {
			System.out.println(" - BUST!");
		}
		else {
			System.out.println(" - Stick");			
		}
		player.setValue(value);
		
		return value == 21;
	}

	private int computeValue(List<Card> hand) {
		
		int value = 0;
		for (Card card : hand) {
			int ord = card.getRank().ordinal();
			if (ord >= Rank.TWO.ordinal() && ord < Rank.JACK.ordinal()) {
				value += ord + 2;
			}
			else if (card.getRank() == Rank.ACE) {
				value += 11;
			}
			else {
				value += 10;
			}
		}
		return value;
	}

	@Override
	public Player declareWinner(Player[] players) {
		// TODO Complete this method 
		return null;
	}

}
