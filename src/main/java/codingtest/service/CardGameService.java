package codingtest.service;

import codingtest.domain.Deck;
import codingtest.domain.Player;

public interface CardGameService {

	/**
	 * Do the initial deal to each player.
	 * @param players - players in game
	 * @param deck - deck to be used
	 */
	void dealHands(Player[] players, Deck deck);
	
	/**
	 * Take a players turn.
	 * If that player's move ends the game return true.
	 * 
	 * @param player - player who has turn
	 * @param deck - card deck used
	 * @return boolean - true if player has won, and game should end.
	 * @throws GameOverException - if service detects game is over
	 */
	boolean takeTurn(Player player, Deck deck) throws GameOverException;
	
	/**
	 * Find and declare the winner.
	 * @param players
	 * @return
	 */
	Player declareWinner(Player[] players);
}
