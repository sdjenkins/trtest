package codingtest.domain;

import org.junit.Assert;
import org.junit.Test;

public class DeckTest {

	@Test
	public void testSuffle() {

		Card[] cards = { new Card(Rank.TWO, Suit.CLUBS),
				new Card(Rank.THREE, Suit.CLUBS),
				new Card(Rank.TWO, Suit.DIAMONDS),
				new Card(Rank.THREE, Suit.DIAMONDS), };

		Deck deck = new Deck(cards);

		Assert.assertTrue(deck.dealNextCard() == cards[0]);
		Assert.assertTrue(deck.dealNextCard() == cards[1]);
		Assert.assertTrue(deck.dealNextCard() == cards[2]);
		Assert.assertTrue(deck.dealNextCard() == cards[3]);
		
		deck = new Deck(cards);
		deck.shuffle();
		
		boolean isOrdered = 
				deck.dealNextCard() == cards[0] &&
				deck.dealNextCard() == cards[1] &&
				deck.dealNextCard() == cards[2] &&
				deck.dealNextCard() == cards[3];

				
		Assert.assertFalse(isOrdered);

	}
	
	@Test
	public void testToString() {
		
		Assert.assertEquals(
				"[ 2♣,  3♣,  4♣,  5♣,  6♣,  7♣,  8♣,  9♣,  10♣,  J♣,  Q♣,  K♣,  A♣,"
				+ "  2♦,  3♦,  4♦,  5♦,  6♦,  7♦,  8♦,  9♦,  10♦,  J♦,  Q♦,  K♦,  A♦,"
				+ "  2♥,  3♥,  4♥,  5♥,  6♥,  7♥,  8♥,  9♥,  10♥,  J♥,  Q♥,  K♥,  A♥,"
				+ "  2♠,  3♠,  4♠,  5♠,  6♠,  7♠,  8♠,  9♠,  10♠,  J♠,  Q♠,  K♠,  A♠]",
				Deck.getStandard52CardDeck().toString()
				);
	}
}
