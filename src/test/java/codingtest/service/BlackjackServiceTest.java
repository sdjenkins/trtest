package codingtest.service;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import codingtest.domain.Card;
import codingtest.domain.Deck;
import codingtest.domain.Player;
import codingtest.domain.Rank;
import codingtest.domain.Suit;

//TODO - add declare winner test

public class BlackjackServiceTest {

	private static final Card[] CARDS = { 
		new Card(Rank.KING, Suit.CLUBS),
		new Card(Rank.TEN, Suit.CLUBS),
		new Card(Rank.FIVE, Suit.DIAMONDS), 
		new Card(Rank.QUEEN, Suit.DIAMONDS),
		new Card(Rank.TWO, Suit.HEARTS),
		new Card(Rank.JACK, Suit.HEARTS),
		new Card(Rank.TWO, Suit.SPADES),
		new Card(Rank.THREE, Suit.CLUBS),
		new Card(Rank.ACE, Suit.CLUBS),
	};
	
	private static BlackjackService service = new BlackjackService();
	
	@Test
	public void testDealHands() {
		

		Deck deck = new Deck(CARDS);
		
		Player[] players = 
			{
				new Player("Fred"),
				new Player("John"),
				new Player("Mary")				
			};
		
		service.dealHands(players, deck);
		
		Assert.assertEquals(CARDS[0], players[0].getHand().get(0));
		Assert.assertEquals(CARDS[1], players[1].getHand().get(0));		
		Assert.assertEquals(CARDS[2], players[2].getHand().get(0));		
	
		Assert.assertEquals(CARDS[3], players[0].getHand().get(1));
		Assert.assertEquals(CARDS[4], players[1].getHand().get(1));		
		Assert.assertEquals(CARDS[5], players[2].getHand().get(1));	
		
		assertEquals(2, players[0].getHand().size());
		assertEquals(2, players[1].getHand().size());
		assertEquals(2, players[2].getHand().size());
		
	}
	
	@Test
	public void testTakeTurn() throws GameOverException {
		
		//TODO - add 21 test
		
		Deck deck = new Deck(CARDS);
		
		Player[] players = 
			{
				new Player("Fred"),
				new Player("John"),
				new Player("Mary")				
			};
		
		service.dealHands(players, deck);
		boolean status = service.takeTurn(players[0], deck);
		Assert.assertEquals(20, players[0].getValue());
		
		status = service.takeTurn(players[1], deck);
		Assert.assertEquals(17, players[1].getValue());
		
		status = service.takeTurn(players[2], deck);
		Assert.assertEquals(26, players[2].getValue());
		
		try {
			service.takeTurn(players[2], deck);
		}
		catch (Exception e) {
			Assert.assertTrue(e instanceof GameOverException);
		}
	}
}
